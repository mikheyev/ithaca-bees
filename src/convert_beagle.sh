#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N bc
#$ -l h_vmem=40G
#$ -l virtual_free=40G

#convert phased vcf files into beagle 3 input files	

for i in ../data/beagle/output/*.vcf.gz; do zcat $i | java -Xmx35g  -jar  /apps/MikheyevU/sasha/beagle_phase/vcf2beagle.jar  ? ../data/beagle/output/`basename $i .vcf.gz` ; done