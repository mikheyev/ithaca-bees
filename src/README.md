# Source code

## call.sh

Use freebayes to call SNPs. Afterward, scaffold VCFs are collected into data/var/raw.vcf

	(grep  ^# tmp/1.vcf ; for i in `seq 1 5644`; do grep -v ^# tmp/$i.vcf; done ) > raw.vcf

### VCF filtering

- These commands are run from data/var/ directory.

Find features that are too close to one another:

    cat raw.vcf | vcffilter -f "QUAL > 20" | mergeBed -i - -d 50 -nms |grep ";" > tooClose50.bed
    cat raw.vcf | vcffilter -f "QUAL > 20" | mergeBed -i - -d 25 -nms |grep ";" > tooClose25.bed

VCFtools leaves multi-nucleotide SNPs, which should also be masked:

    grep -v ^# raw.vcf |awk '(length($4)>1 && length($5)>1 && length($4) == length($5)) {print $1"\t"$2"\t"($2+length($4))}' >mnsnps.bed

Individuals with too much missing data should be removed:

    vcftools --vcf raw.vcf --missing-indv
    awk 'NR>1 && $5<.3 {print $1}' out.imiss > keep.txt
    cat keep.txt | grep -vc ^HB
    32

Now, apply all the filters

    vcftools --vcf raw.vcf --minQ 60 --max-alleles 2 --max-missing 0.7  --remove-indels --maf 0.1 --keep keep.txt --exclude-bed mnsnps.bed --recode --out filtered

This leaves 1,656,192 SNPs

Filter out SNPs that are too close to other SNPs

	vcftools --vcf filtered.recode.vcf --exclude-bed tooClose25.bed --recode --out tooClose25

This leaves 255,109 SNPs

Repeat mask SNPs:

	qsub -j y -cwd -q genomics -l h_vmem=10G -l virtual_free=10G <<< '. $HOME/.bashrc; (grep ^# filtered.recode.vcf ;intersectBed -v -a filtered.recode.vcf -b ../../../ref/repeats.gff) > snps.repeatmasked.vcf '

This leaves 1,031,289 SNPs

	vcftools --vcf snps.repeatmasked.vcf --exclude-bed tooClose25.bed --recode --out good

Kept 181,352 SNPs.


### ANGSD

Generate bim file

    cat data/var/filtered.recode.vcf | grep -v ^# | awk -v OFS="\t"  '{print $1,".",".",$2,$4,$5}' > data/angsd/snps.bim

We then compute major and minor alleles and LRT on the filtered.recode.vcf using `angsd.sh`, using -t 1,2 to get MAFs for old and modern populations. We use `angsd_lrt.sh` to compute changes significant changes in old and modern populations.

#### Filters for estimating migration and drift

Here we don't want to filter out the rare alleles

	vcftools --vcf raw.vcf --minQ 60 --max-alleles 2 --max-missing 0.7  --remove-indels  --keep keep.txt --exclude-bed mnsnps.bed --recode bed --out filtered_no_maf_filter
	qsub -j y -cwd -q genomics -l h_vmem=10G -l virtual_free=10G <<< '. $HOME/.bashrc; (grep ^# filtered_no_maf_filter.recode.vcf ;intersectBed -v -a filtered_no_maf_filter.recode.vcf -b ../../../ref/repeats.gff) > filtered_no_maf_filter.repeatmasked.vcf '
	vcftools --vcf filtered_no_maf_filter.repeatmasked.vcf --exclude-bed tooClose25.bed --recode --out filtered_no_maf_filter2
	grep -v ^# filtered_no_maf_filter2.recode.vcf | awk -v OFS="\t"  '{print $1,".",".",$2,$4,$5}' > ../angsd/snps_no_maf_filter.bim

We then run `angsd_no_maf_filter.sh` to compute allele frequencies for each population.


### BEAGLE

##### Note: This analysis is included in the repository for historical reasons, but we did not use it in the end. Test using beagle on bee data in another project, where we could estimate switching errors exactly, were disappointing.

We need to adjust the missing data field to make it work with beagle

	cat data/var/good.recode.vcf | sed 's/\s\.:/\t\.\/\.:/g' > tmp
	mv tmp data/var/good.recode.vcf

Now run `beagle.sh`.

Compute allele frequencies from BEAGLE


```
(zcat output/Group1.1.vcf.gz |grep ^# ; for i in `grep ^Group ../var/good.recode.vcf |grep -v ^GroupUN | cut -f1 |uniq` ; do zcat output/$i.vcf.gz | grep -v ^# ; done ) > beagle.vcf
vcftools --vcf beagle.vcf --freq --out modern --keep ../../meta/modern.txt
vcftools --vcf beagle.vcf --freq --out old --keep ../../meta/old.txt

paste old.frq modern.frq |tr ":" "\t" | awk -v OFS=, 'BEGIN {print "chrom,pos,old,modern"} NR>1 {print $1,$2,1-$6,1-$14}' > freqs.csv
```
Convert vcf from BEAGLE 4 to beagle format using `convert_beagle.sh`:

	for i in ../data/beagle/output/*.vcf.gz; do zcat $i | java -Xmx35g  -jar  /apps/MikheyevU/sasha/beagle_phase/vcf2beagle.jar  ? ../data/beagle/output/`basename $i .vcf.gz` ; done

Then perform association testing using BEALGE 3

Finally, merge all the data into a csv

    echo "chrom,pos,p" > beagle.csv
    cat *.bgl.pval |grep -v ^Marker | tr ":" "\t" |awk -v OFS=, 'length($3)==1 {print $1,$2, $4}' >> beagle.csv

### Estimates of heterozygosity

	vcftools --vcf good.recode.vcf --het --minGQ 20 --out modern --keep ../../meta/modern.txt
	vcftools --vcf good.recode.vcf --het --minGQ 20 --out old --keep ../../meta/old.txt

 `convert_rehh.sh` was used to attempt a REHH analysis, but that proved unreliable due to potential problems with beagle phasing.

## Mitochondrial analysis

### Masking numts

We need to find regions of the mitochondria that might have nuclear analogs and mask them

	blastn -query mito.fa -db Amel_4.5.AGP.linearScaffold -outfmt 6 -out mito_vs_amel45.txt
	awk -v OFS="\t" '{if ($8 < $7) print $1, $8-1,$7-1 ; else print $1, $7-1,$8-1}' mito_vs_amel45.txt |sort -k2,2 -n |mergeBed

This mask everything but a few bases, so we go without masking.


### List of haplotypes

Having called SNPs and indels using Freebayes and UnifiedGenotypers, using `call_mito_fb.sh` and `call_mito_gatk.sh`, then intersecting the data:

	(grep ^# mito_raw_gatk.vcf ;awk '$6>40' mito_raw_freebayes.vcf |intersectBed -b - -a mito_raw_gatk.vcf -f 1.0) > mito_intersect.vcf
	vcftools --vcf mito_intersect.vcf --recode --out mito_intersect --max-missing 1 --max-alleles 2 --remove-indels
	cat mito_intersect.recode.vcf | vcf-to-tab | python -c "import sys; print('\n'.join(c[0]+' ' + ''.join(c[1:]) for c in zip(*(l.split() for l in sys.stdin.readlines() if l.strip()))))"  |awk 'NR>3' > mito_intersect_haplotypes.txt

