#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N st
#$ -l h_vmem=10G
#$ -l virtual_free=10G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

old_IFS=$IFS
IFS=$'\n'
a=(`cat ../data/var/keep.txt`) #64
IFS=$old_IFS
f=${a[$SGE_TASK_ID-1]}
infiles=`grep $f ../meta/bams.list | tr "\n" " "`

novosort -t $TMP --ram 8 -i -o ../data/alignments/$f.bam $infiles