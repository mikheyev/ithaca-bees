#!/bin/bash
#$ -q genomics
#$ -j y
#$ -cwd
#$ -N cv
#$ -l h_vmem=100G
#$ -l virtual_free=100G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

ref=./../../ref/Amel_4.5.AGP.linearScaffold.fa

alias GA="java -Xmx90g -Djava.io.tmpdir=/genefs/MikheyevU/temp -jar /apps/MikheyevU/sasha/GATK/GenomeAnalysisTK.jar"

GA \
	-T DepthOfCoverage \
	-R $ref \
	-I ../meta/bams.list \
	-ct 16 \
	-omitBaseOutput `#Do not output depth of coverage at each base` \
	-o ../data/GATK/coverage `#output base` \
	--omitDepthOutputAtEachBase `#Do not output depth of coverage at each base` \
	-omitIntervals `#Do not calculate per-interval statistics`
	


