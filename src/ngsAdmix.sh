#!/bin/bash
#$ -q shortP
#$ -j y
#$ -cwd
#$ -N amix
#$ -pe smp 2
#$ -l h_vmem=4G
#$ -l virtual_free=4G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

K=$SGE_TASK_ID
#rep=5

/apps/MikheyevU/popgen/NGSadmix -P $NSLOTS -likes ../data/angsd/bgl/all.beagle.gz  -K $K -outfiles ../data/angsd/admix/$K"_"$rep



