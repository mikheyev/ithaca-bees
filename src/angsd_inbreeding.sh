#!/bin/bash
#$ -q shortP
#$ -j y
#$ -cwd
#$ -N ag
#$ -pe smp 16
#$ -l h_vmem=3G
#$ -l virtual_free=3G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

module load zlib/1.2.8

ANGSD=/apps/MikheyevU/sasha/ngsTools/angsd  
ref=../../ref/Amel_4.5.AGP.linearScaffold.fa
OUTDIR=./../data/angsd/f
INDIR=./../data/angsd
N_IND=32
#NSLOTS=1
# Calculate SFS for modern and old bees, using reference allele as major
# 1 -- old
# !=1 -- modern

if [ $SGE_TASK_ID -eq 1 ]; then
    echo "computing old populations"
    pop=old
else
    echo "computing modern populations"
    pop=modern
fi

#get random subset of data for inbreeding coefficient estimation
rm $pop.temp*
cat $INDIR/snps.txt | grep -v GroupUN | shuf |head  -n 5000 > $pop.temp.chosen
awk 'FNR==NR {x[$1"_"$2]=$0;next} x[$1"_"$2] {print };' $pop.temp.chosen $INDIR/snps.txt > $pop.temp
cat $pop.temp |cut -f1 | uniq > $pop.temp.regions

#compute genotype likelihoods
$ANGSD/angsd  -ref $ref -bam $INDIR/$pop.txt -out $OUTDIR/$pop -doGeno 32 -doPost 1 -doMaf 2 -doMajorMinor 4 -GL 4 -doCounts 1 -nThreads $NSLOTS  -doGlf 3 -sites  $pop.temp -rf $pop.temp.regions
#-r Group1.1: # for testing
# estimate inbreeding coefficients
N_SITES=$((`zcat $OUTDIR/$pop.mafs.gz | wc -l`-1))

zcat $OUTDIR/$pop.glf.gz | $ANGSD/../ngsF/ngsF -n_threads $NSLOTS -n_ind $N_IND -n_sites $N_SITES -glf - -min_epsilon 0.001 -out $OUTDIR/$pop.approx_indF -approx_EM -seed 12345 -init_values r

zcat $OUTDIR/$pop.glf.gz| $ANGSD/../ngsF/ngsF -n_threads $NSLOTS -n_ind $N_IND -n_sites $N_SITES -glf - -min_epsilon 0.001 -out $OUTDIR/$pop.indF -init_values $OUTDIR/$pop.approx_indF.pars

# incorporate these estimates in the calculation of genotype posterior probabilities

$ANGSD/angsd  -sites $INDIR/snps.txt -anc $ref -ref $ref -bam $INDIR/$pop.txt -out $OUTDIR/$pop.indF -doGeno 32 -doPost 1 -doMaf 1 -doMajorMinor 4 -GL 4 -doCounts 1 -nThreads $NSLOTS  -doSaf 2 -indF $OUTDIR/$pop.indF -rf ../data/angsd/regions.txt
