#!/bin/bash
#$ -q genomics
#$ -j y
#$ -cwd
#$ -N bee_hct
#$ -l h_vmem=100G
#$ -l virtual_free=100G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp
MAXMEM=90

ref=../../ref/mito.fa
alias GA="java -Xmx"$MAXMEM"g -Djava.io.tmpdir=/genefs/MikheyevU/temp -jar /apps/MikheyevU/sasha/GATK/GenomeAnalysisTK.jar"

GA -nct 20 \
    -T UnifiedGenotyper \
    -R $ref \
    -I ../meta/mito.list \
    --sample_ploidy 1 \
    -glm BOTH \
    -stand_emit_conf 60 \
    -stand_call_conf 60 \
    --genotyping_mode DISCOVERY \
    -o ../data/var/mito_raw_gatk.vcf
