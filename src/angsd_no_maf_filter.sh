#!/bin/bash
#$ -q long
#$ -j y
#$ -cwd
#$ -N ag
#$ -l h_vmem=30G
#$ -l virtual_free=30G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

ref=../../ref/Amel_4.5.AGP.linearScaffold.fa

# Calculate allele frequencies, genotype frequencies for modern and old bees, using reference allele as major

# 1 -- old
# 2 -- modern


if [ $SGE_TASK_ID -eq 1 ]; then
	/apps/MikheyevU/popgen/angsd/angsd -filter ../data/angsd/snps_no_maf_filter.bim -ref $ref -bam ../data/angsd/old.txt -out ../data/angsd/old_no_maf_filter -doGeno 32 -doPost 1 -doMaf 2 -doMajorMinor 4 -GL 4 -doCounts 1 -nThreads 12
else
	/apps/MikheyevU/popgen/angsd/angsd -filter ../data/angsd/snps_no_maf_filter.bim -ref $ref -bam ../data/angsd/modern.txt -out ../data/angsd/modern_no_maf_filter -doGeno 32 -doPost 1 -doMaf 2 -doMajorMinor 4 -GL 4 -doCounts 1 -nThreads 12
fi

