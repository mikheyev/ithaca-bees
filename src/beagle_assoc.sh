#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N bg
#$ -l h_vmem=30G
#$ -l virtual_free=30G

. $HOME/.bashrc

a=(../data/beagle/output/*.bgl.gz) # 326
f=${a[$SGE_TASK_ID-1]}
o="../data/beagle/output/"$(basename $f .bgl.gz)

gunzip -c $f | head -1  > $o.bgl
echo A population `for i in $(seq 1 64); do echo -ne "2 "; done` `for i in $(seq 1 64); do echo -ne "1 "; done` >> $o.bgl
gunzip -c $f | awk 'NR>1 ' >> $o.bgl  

java  -Djava.io.tmpdir=/genefs/MikheyevU/temp  -Xmx20000m  -jar /apps/MikheyevU/sasha/beagle_phase/beagle.jar data=$o.bgl trait=population out=`basename $f .bgl.gz`

