#!/bin/bash
#$ -q long
#$ -j y
#$ -cwd
#$ -N fb
#$ -l h_vmem=40G
#$ -l virtual_free=40G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

ref=./../../ref/mito.fa

#/bucket/MikheyevU/apps/freebayes/bin/freebayes --populations ./../meta/pops.txt -p 1 -f $ref --genotype-qualities --bam-list ./../meta/mito.list -v ./../data/var/mito_raw.vcf

 novosort -a --compression 0 `cat ./../meta/mito.list | tr "\n" " "` | samtools view -F4 -q30 -u -s 0.1 - | /bucket/MikheyevU/apps/freebayes/bin/freebayes --stdin --populations ./../meta/pops.txt -p 1 -f $ref --genotype-qualities  -v ./../data/var/mito_raw_freebayes.vcf

