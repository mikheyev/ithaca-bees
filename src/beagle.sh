#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N bg
#$ -l h_vmem=40G
#$ -l virtual_free=40G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

old_IFS=$IFS
IFS=$'\n'
a=(`cat ../meta/placed_scaffolds.txt`) #340
IFS=$old_IFS
f=${a[$SGE_TASK_ID-1]}

#phase bee data, and infer ibd

java  -Djava.io.tmpdir=/genefs/MikheyevU/temp -Xmx35g  -jar /apps/MikheyevU/sasha/beagle_phase/b4.r1274.jar gtgl=../data/var/good.recode.vcf  \
ibd=true chrom=$f out=../data/beagle/output/$f
