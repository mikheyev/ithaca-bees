#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N fb
#$ -l h_vmem=16G
#$ -l virtual_free=16G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

ref=./../../ref/Amel_4.5.AGP.linearScaffold.fa

 old_IFS=$IFS
 IFS=$'\n'
 a=(`cat ./../meta/scaffolds.txt`) #5644
 IFS=$old_IFS
 f=${a[$SGE_TASK_ID-1]}


/bucket/MikheyevU/apps/freebayes/bin/freebayes --populations ./../meta/pops.txt --theta 0.002 -f $ref --genotype-qualities \
  --region $f --bam-list ./../meta/bams.txt -v ./../data/var/tmp/$SGE_TASK_ID.vcf


