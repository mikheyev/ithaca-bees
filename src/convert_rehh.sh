#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N rh
#$ -l h_vmem=4G
#$ -l virtual_free=4G

#convert phased vcf files into rehh input files	
for i in ../data/beagle/output/*vcf.gz
do
	base=`basename $i .vcf.gz`
	zcat $i | grep -v ^#  | cut -f10-41 | \
        awk 'BEGIN {for(i=1;i<=32;i++) {h[i".1"]="";h[i".2"]="";}} {for(i=1;i<=32;i++) {split($i,a,":"); split(a[1],b,"|");h[(i)".1"]=h[(i)".1"]"\t"b[1]; 
        h[(i)".2"]=h[(i)".2"]"\t"b[2]}} END { for(i in h) print i,h[i]}' >../data/rehh/modern/$base.hap
	zcat $i | awk '$0!~/^#/ {print $2,1,$2,0,1}'   > ../data/rehh/modern/$base.map
	zcat $i | grep -v ^#  | cut -f42- | \
        awk 'BEGIN {for(i=1;i<=22;i++) {h[i".1"]="";h[i".2"]="";}} {for(i=1;i<=22;i++) {split($i,a,":"); split(a[1],b,"|");h[(i)".1"]=h[(i)".1"]"\t"b[1]; 
        h[(i)".2"]=h[(i)".2"]"\t"b[2]}} END { for(i in h) print i,h[i]}' >../data/rehh/old/$base.hap
	zcat $i | awk '$0!~/^#/ {print $2,1,$2,0,1}'   > ../data/rehh/old/$base.map
done
