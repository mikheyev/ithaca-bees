#!/bin/bash
#$ -q short
#$ -j y
#$ -cwd
#$ -N ag
#$ -l h_vmem=4G
#$ -l virtual_free=4G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

module load zlib/1.2.8

ANGSD=/apps/MikheyevU/sasha/ngsTools/angsd  
ref=../../ref/Amel_4.5.AGP.linearScaffold.fa
OUTDIR=../data/angsd/f
INDIR=../data/angsd/f
N_IND=32
#NSLOTS=1

#find common sites
gunzip -c $INDIR/modern.indF.saf.pos.gz > $OUTDIR/modern.saf.pos
gunzip -c $INDIR/old.indF.saf.pos.gz > $OUTDIR/old.saf.pos
awk 'FNR==NR {x[$1"_"$2]=NR; next} x[$1"_"$2] {print x[$1"_"$2]; print FNR > "/dev/stderr"}' $OUTDIR/old.saf.pos $OUTDIR/modern.saf.pos >$OUTDIR/old.pos 2>$OUTDIR/modern.pos 
rm $OUTDIR/old.saf.pos $OUTDIR/modern.saf.pos

#generate saf files with common sites
IN_SITES=$((`zcat $INDIR/old.indF.saf.pos.gz | wc -l`-1))
OUT_SITES=$((`cat $OUTDIR/old.pos |wc -l`-1))
$ANGSD/../ngsUtils/GetSubSfs -infile $INDIR/old.indF.saf -posfile $OUTDIR/old.pos -nind 32 -nsites $IN_SITES -len $OUT_SITES -outfile $OUTDIR/old.fix.saf
IN_SITES=$((`zcat $INDIR/modern.indF.saf.pos.gz | wc -l`-1))
$ANGSD/../ngsUtils/GetSubSfs -infile $INDIR/modern.indF.saf -posfile $OUTDIR/modern.pos -nind 32 -nsites $IN_SITES -len $OUT_SITES -outfile $OUTDIR/modern.fix.saf
rm $OUTDIR/fst.txt
$ANGSD/../ngsPopGen/ngsFST -postfiles  $OUTDIR/old.fix.saf $OUTDIR/modern.fix.saf -nind 32 32 -nsites $OUT_SITES -outfile $OUTDIR/fst.txt -islog 1

rm  $OUTDIR/stat.txt
$ANGSD/../ngsPopGen/ngsStat -npop 2 -postfiles $OUTDIR/old.fix.saf $OUTDIR/modern.fix.saf -nsites $OUT_SITES -iswin 1 -nind 32 32 -islog 1 -outfile $OUTDIR/stat.txt -isfold 0 -verbose 0 -block_size 1
