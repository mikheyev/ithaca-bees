#!/bin/bash
#$ -q genomics
#$ -j y
#$ -cwd
#$ -N pca
#$ -l h_vmem=20G
#$ -l virtual_free=20G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

# compute genotype posterior probabilities
/apps/MikheyevU/popgen/angsd/angsd0.574/angsd -nThreads 12 -rf ../meta/pca_scaffolds.txt -sites ../data/angsd/snps.txt -bam ../meta/merged.list -doGeno 32 -doPost 1 -doMaf 2 -doMajorMinor 3 -GL 4 -doCounts 1 -out ../data/angsd/all 

find ../data/angsd/all.covar -delete

gunzip -f ../data/angsd/all.geno.gz

nSites=$(( `zcat ../data/angsd/all.mafs.gz |wc -l` -1))

# compute pca
/apps/MikheyevU/popgen/ngsTools/bin/ngsCovar -probfile ../data/angsd/all.geno -outfile ../data/angsd/all.covar -nind 64 -nsites $nSites -call 0 -minmaf 0.05 
