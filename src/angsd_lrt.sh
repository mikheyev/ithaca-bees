#!/bin/bash
#$ -q long
#$ -j y
#$ -cwd
#$ -N aassoc
#$ -l h_vmem=40G
#$ -l virtual_free=40G

. $HOME/.bashrc

export TEMPDIR=/genefs/MikheyevU/temp
export TEMP=/genefs/MikheyevU/temp
export TMP=/genefs/MikheyevU/temp

ref=../../ref/Amel_4.5.AGP.linearScaffold.fa 

# compute LRT statistics for SNP frequency changes in modern and and old bees

cat ../data/angsd/old.txt ../data/angsd/modern.txt > ../data/angsd/both.txt

for i in `seq 1 32`; do echo 0; done > ../data/angsd/cases.txt
for i in `seq 1 32`; do echo 1; done >> ../data/angsd/cases.txt

/apps/MikheyevU/popgen/angsd/angsd -filter ../data/angsd/snps.bim -ref $ref -bam ../data/angsd/both.txt -doAsso 1 -yBin ../data/angsd/cases.txt -doGeno 32 -doPost 1 -doMaf 2 -doMajorMinor 4 -GL 4 -doCounts 1 -nThreads 12 -doSNP 1 -out ../data/angsd/both


