# Analysis of population genetic changes in the Ithaca bee population

## File organization

### Source code

[src/](src/)

### Processed data files. 

This folder is too large to include in the git repository

data/

### supporting meta-data for analysis scripts, such as scaffold lists, file location, etc

This is not a particularly interesting directory for the human reader. It has lists used by the scripts in `./src/`, many of which are used to parallelize scripts on OIST's tombo cluster

meta/
